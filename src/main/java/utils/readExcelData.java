package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class readExcelData {
	
	
	public static Object[][] readData(String dataSheetName) throws IOException
	{
		
		XSSFWorkbook wb=new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
		XSSFSheet sheet=wb.getSheetAt(0);
		
		int rowCount,columnCount;
		rowCount=sheet.getLastRowNum();	
		columnCount=sheet.getRow(0).getLastCellNum();
		Object[][] data=new Object[rowCount][columnCount];
								
		for (int j = 1; j <=rowCount; j++) {
				
			XSSFRow row=sheet.getRow(j);
			 columnCount = row.getLastCellNum();
			
					for (int i = 0; i < columnCount; i++) {
				XSSFCell cell = row.getCell(i);
				
				try {
					String value = cell.getStringCellValue();
					System.out.println(value);
					
					data[j-1][i]=value;
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					System.out.println("");
				}
			} 
		}
		wb.close();
		return data;
		
	}
}