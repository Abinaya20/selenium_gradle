package wdMethods;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import utils.readExcelData;

public class ProjectMethods extends SeMethods{
	
	public String dataSheetName;
	
	//@BeforeTest
	//public void beforeTest() {
		//System.out.println("@BeforeTest");
	//}
	
	//@BeforeClass
	//public void beforeClass() {
		//System.out.println("@BeforeClass");
	//}
	
	
	@BeforeMethod()
	public void login() {
		beforeMethod();
		startApp("chrome", "http://leaftaps.com/opentaps");
		}
	
	@AfterMethod()
	public void closeApp() {
		closeBrowser();
	}
	@AfterClass()
	public void afterClass() {
		System.out.println("@AfterClass");
	}
	@AfterTest()
	public void afterTest() {
		System.out.println("@AfterTest");
	}
	@AfterSuite()
	public void afterSuite() {
		endResult();
	}
	
	@BeforeSuite()
	public void beforeSuite() {
		startResult();
	}
	
	
	
	@DataProvider(name="fetchData")
	public Object[][] getData() throws IOException
	{
					return readExcelData.readData(dataSheetName);
				
		}
	}





