package testcase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.HomePage;
import pageObjects.LoginPage;
import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {

	@BeforeTest
	public void setData()
	{
		testCaseName="Login_Logout";
		testDesc="Using POM";
		author="Abinaya";
		category="smoke";
		dataSheetName="createLead";
	}

	@Test(dataProvider="fetchData")
	public void LoginLogoutCase(String cName,String fName ,String lName )
	{
		new LoginPage().enterUsername("DemoSalesManager").enterPassword("crmsfa").clickLogin().clickCRMSFA().clickLeads().clickCreateLead().enterCompanyName(cName).enterFirstName(fName).enterLastName(lName).clickCreatelead().viewFirstName();
	}
}

