package testcase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.LoginPage;
import wdMethods.ProjectMethods;

public class LoginLogout extends ProjectMethods {
	
	@BeforeTest
	public void setData()
	{
		testCaseName="Login_Logout";
		testDesc="Using POM";
		author="Abinaya";
		category="smoke";
		dataSheetName="loginLogout";
	}

	@Test(dataProvider="fetchData")
	public void LoginLogoutCase(String uName,String password)
	{
		new LoginPage().enterUsername(uName).enterPassword(password).clickLogin();
	}
}
