package testcase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.LoginPage;
import pageObjects.MergeLeadPage;
import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods {
	@BeforeTest
	public void setData()
	{
		testCaseName="Login_Logout";
		testDesc="Using POM";
		author="Abinaya";
		category="smoke";
		dataSheetName="mergeLead";
	}

	@Test(dataProvider="fetchData")
	public void LoginLogoutCase(String fName) throws InterruptedException
	{
		new LoginPage().enterUsername("DemoSalesManager").enterPassword("crmsfa").clickLogin().clickCRMSFA().clickLeads().clickMergeLead()//.clickFromImage().enterFirstName(fName).clickFindLead().clickFirstRow().
		.clickToImage().enterFirstName(fName).clickFindLead().clickSecondRow().clickMergeButton().viewFirstName();
	}
}

