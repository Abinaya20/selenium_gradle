package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;


	public class CreateLeadPage extends ProjectMethods {
		
		public CreateLeadPage()
		{
			PageFactory.initElements(driver, this);
		}
		
		@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
		@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
		@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLastName;
		//@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement elePhone;
		//@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleEmail;
		@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleCreatelead;
		
		
		public CreateLeadPage enterCompanyName(String cName)
		{
			//type(eleCompanyName, cName);
			eleCompanyName.sendKeys(cName);
			return this;			
		}
		
		public CreateLeadPage enterFirstName(String fName)
		{
			type(eleFirstName, fName);
			return this;			
		}
		
		public CreateLeadPage enterLastName(String lName)
		{
			type(eleLastName, lName);
			return this;			
		}
		
		public ViewLeadPage clickCreatelead()
		{
			click(eleCreatelead);
			return new ViewLeadPage();
			
		}
		
		
}
