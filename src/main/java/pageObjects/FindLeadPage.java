package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods {
	
	public FindLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.NAME,using="firstName") WebElement eleFirstName;
	@FindBy(how=How.CLASS_NAME,using="x-btn-text") WebElement eleFindLead;
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])") WebElement eleFirstRow;
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[2]") WebElement eleSecondRow;
	
	public FindLeadPage enterFirstName(String fName) throws InterruptedException 
	
	{
		Thread.sleep(2000);
		eleFirstName.sendKeys(fName);
	return this;		
	}
	
	public FindLeadPage clickFindLead() throws InterruptedException
		{
	Thread.sleep(2000);
	WebDriverWait wait=new WebDriverWait(driver,10);
	click(eleFindLead);
	
	
		return this;		
	}
	
	public MergeLeadPage clickFirstRow() throws InterruptedException
	{
		click(eleFirstRow);
		Thread.sleep(2000);
		WebDriverWait wait=new WebDriverWait(driver,10);
		switchToWindow(0);
		Thread.sleep(2000);
		return new MergeLeadPage();
		
	}
	
	public MergeLeadPage clickSecondRow()
	{
		click(eleSecondRow);
		
		return new MergeLeadPage();
		
	}
	
	
	
}

