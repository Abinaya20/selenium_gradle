package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods {
	
	public MergeLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="//img[@alt='Lookup']") WebElement eleFromImage;
	@FindBy(how=How.XPATH,using="(//img[@alt='Lookup'])[2]") WebElement eleToImage;
	@FindBy(how=How.LINK_TEXT,using="Merge") WebElement eleMergeButton;
	
	
	
	public FindLeadPage clickFromImage() throws InterruptedException
	{
		click(eleFromImage);
		Thread.sleep(2000);
		switchToWindow(1);
		Thread.sleep(2000);
		return new FindLeadPage();
	}
	
	/*public FindLeadPage switchToFindLeadWindow()
	{
		switchToWindow(0);
		return new FindLeadPage();
	}*/
	
	public FindLeadPage clickToImage() throws InterruptedException
	{
		
		Thread.sleep(2000);
		WebDriverWait wait=new WebDriverWait(driver,10);
		click(eleToImage);
		return new FindLeadPage();
		
	}
	public ViewLeadPage clickMergeButton() throws InterruptedException
	{
		click(eleMergeButton);
		//Thread.sleep(2000);
		switchToWindow(0);
		Thread.sleep(2000);
		acceptAlert();
		return new ViewLeadPage();
	}
	
}
	